package ru.aartamonov.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.aartamonov.repository.PhraseRepository;


class PhraseServiceImplTest {

    private final static String MSG = "No doubt!";
    private PhraseService phraseService;
    private PhraseRepository phraseRepository;

    @BeforeEach
    void setUp() {
        phraseRepository = Mockito.mock(PhraseRepository.class);
        phraseService = new PhraseServiceImpl(phraseRepository);
    }

    @Test
    void getPhraseShouldReturnMessage() {
        Mockito.doReturn(MSG).when(phraseRepository).get();

        var result = phraseService.getPhrase();

        Assertions.assertThat(result).isNotBlank();
        Assertions.assertThat(result).isEqualTo(MSG);
    }

    @Test
    void putPhraseShouldReturnModifiedMessage() {
        Assertions.assertThat(phraseService.setPhrase(MSG)).contains(MSG);
    }

    @Test
    void putPhraseShouldReturnExceptionOnEmptyPhrase() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> phraseService.setPhrase(null));
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> phraseService.setPhrase(""));
    }

}