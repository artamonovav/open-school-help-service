package ru.aartamonov.servlet;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.aartamonov.service.PhraseService;
import ru.aartamonov.service.PhraseServiceImpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.mockito.Mockito.times;


public class PhraseServletTest {

    private final static String MSG = "No doubt!";
    private final static String RETURN_MSG = "Phrase added: %s".formatted(MSG);
    private final static String PARAM_NAME = "phrase";
    private PhraseServlet phraseServlet;
    private PhraseService phraseService;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private StringWriter responseWriter;

    @BeforeEach
    void setUp() throws IOException {
        phraseService = Mockito.mock(PhraseServiceImpl.class);
        phraseServlet = new PhraseServlet(phraseService);
        request = Mockito.mock(HttpServletRequest.class);
        response = Mockito.mock(HttpServletResponse.class);
        responseWriter = new StringWriter();

        var writer = new PrintWriter(responseWriter);
        Mockito.doReturn(writer).when(response).getWriter();
    }

    @Test
    void shouldReturnMessageWhenDoGet() {
        Mockito.doReturn(MSG).when(phraseService).getPhrase();

        Assertions.assertThatNoException().isThrownBy(() -> phraseServlet.doGet(request, response));
        Assertions.assertThat(responseWriter.toString()).isEqualTo(MSG);
        Mockito.verify(phraseService, times(1)).getPhrase();
    }

    @Test
    void shouldAddAndReturnModifiedInputMessage() {
        Mockito.doReturn(RETURN_MSG).when(phraseService).setPhrase(Mockito.anyString());
        Mockito.doReturn(MSG).when(request).getParameter(PARAM_NAME);

        Assertions.assertThatNoException().isThrownBy(() -> phraseServlet.doPost(request, response));
        Assertions.assertThat(responseWriter.toString()).isEqualTo(RETURN_MSG);
        Mockito.verify(phraseService, times(1)).setPhrase(MSG);
    }

}
