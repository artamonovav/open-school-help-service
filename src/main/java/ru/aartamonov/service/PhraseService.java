package ru.aartamonov.service;

public interface PhraseService {

    String getPhrase();

    String setPhrase(String phrase);

}
