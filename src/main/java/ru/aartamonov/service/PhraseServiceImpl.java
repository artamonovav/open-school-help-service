package ru.aartamonov.service;

import ru.aartamonov.repository.PhraseRepository;

public class PhraseServiceImpl implements PhraseService {

    private final PhraseRepository phraseRepository;

    public PhraseServiceImpl(PhraseRepository phraseRepository) {
        this.phraseRepository = phraseRepository;
    }

    @Override
    public String getPhrase() {
        return phraseRepository.get();
    }

    @Override
    public String setPhrase(String phrase) {
        if (phrase == null || phrase.isBlank()) {
            throw new IllegalArgumentException("Phrase is empty");
        }
        phraseRepository.put(phrase);
        return "Phrase added: %s".formatted(phrase);
    }

}
