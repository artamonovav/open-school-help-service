package ru.aartamonov.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ru.aartamonov.repository.PhraseRepositoryInMemoryImpl;
import ru.aartamonov.service.PhraseService;
import ru.aartamonov.service.PhraseServiceImpl;

import java.io.IOException;

public class PhraseServlet extends HttpServlet {

    private static final String PARAM_NAME = "phrase";
    private static final String CONTENT_TYPE = "text/plain";

    private PhraseService phraseService;

    public PhraseServlet() {
    }

    public PhraseServlet(PhraseService phraseService) {
        this.phraseService = phraseService;
    }

    @Override
    public void init() throws ServletException {
        if (this.phraseService == null) {
            var phraseRepository = new PhraseRepositoryInMemoryImpl();
            phraseService = new PhraseServiceImpl(phraseRepository);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType(CONTENT_TYPE);
        resp.getWriter().append(phraseService.getPhrase());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType(CONTENT_TYPE);
        resp.getWriter().append(phraseService.setPhrase(req.getParameter(PARAM_NAME)));
    }

}
