package ru.aartamonov.repository;

public interface PhraseRepository {

    String get();

    void put(String phrase);

}
