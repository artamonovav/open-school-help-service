package ru.aartamonov.repository;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class PhraseRepositoryInMemoryImpl implements PhraseRepository {

    private final List<String> phrases = new CopyOnWriteArrayList<>();
    private final Random random = new Random();

    public PhraseRepositoryInMemoryImpl() {
        phrases.add("Делу время, а потехе час!");
        phrases.add("Без труда не вынешь и рыбку из пруда!");
        phrases.add("Код сам себя не напишет!");
    }

    @Override
    public String get() {
        return getRandomPhrase();
    }

    @Override
    public void put(String phrase) {
        phrases.add(phrase);
    }

    private String getRandomPhrase() {
        if (phrases.isEmpty()) {
            return "";
        }
        return phrases.get(random.nextInt(phrases.size()));
    }

}
